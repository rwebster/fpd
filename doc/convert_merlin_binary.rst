.. _convert_merlin_binary:

********************************************
Accessing and Converting Merlin Binary Files
********************************************

In (most) electron microscopes with a Medipix3 detector, the data is acquired
using the Merlin readout system. This outputs a header file and the image data
in a one or more binary files. These can be converted into a more convenient
HDF5-based EMD (https://emdatasets.com/format) compatible format by using the
:py:class:`~fpd.fpd_file.MerlinBinary` class. The class also allows quick access
to the data without conversion.

To raw data is stored flat on disk, so to navigate and store the data in the correct
shape, the scan parameters can be supplied. These are not stored in the Merlin files,
but can be parsed (along with most other scan information) from a Digital Micrograph
(DM) image acquired during the scan. If a DM file is not recorded, the information
can be specified as parameter, either manually or by parsing it from another image.


Using a DM File
===============
With a DM file, the class can be initialised using the following:

* Binary file: ``medipixfile.mib``
* Header file: ``medipixfile.hdr``
* DM-file : ``dmfile.dm3``

.. code-block:: python

    >>> from fpd.fpd_file import MerlinBinary
    >>> mb = MerlinBinary(binfns='medipixfile.mib', hdrfn='medipixfile.hdr', dmfns=['dmfile.dm3'])

One or more DM files can be specified and all files will be parsed and stored
as EMD groups and in raw binary form, allowing the native DM file to be extracted
unmodified and read in Digital Micrograph. Only the first DM file is used for the
scan information.


Without a DM File
=================
If you do not have any DM files, the scan parameters can be specified through the
``scanXalu`` and ``scanYalu`` arguments which are tuples containing for each axis
the `axis`, the label, and units. If `axis` is an integer, it is interpreted
as the axis length and a new integer axis is generated. Otherwise, `axis`
should be an iterable that will be used as the axis directly.

.. code-block:: python

    >>> import numpy as np
    >>> mb = MerlinBinary(
    ...         binfns='medipixfile.mib', hdrfn='medipixfile.hdr',
    ...         scanXalu=(128, 'xaxis', 'pixels'), scanYalu=(np.arange(256)*0.1, 'yaxis', 'nm'))

If another image has been recorded with axis information and it can be read by
HyperSpy, it can be parsed for the axes information using:

.. code-block:: python

    >>> from hyperspy.io import load
    >>> im = load('image_filename')
    >>> scanXalu, scanYalu = [(m.axis, m.name, m.units) for m in im.axes_manager.signal_axes]


Specifying the detector axes
============================
In most systems, it is not trivial to determine the detector axes properties and, often, a
calibration based upon known image dimensions is used. For example, in STEM the brightfield
disc may be used to set the reciprocal space pixel size. 

Accessing the data pre-conversion using the memmory mapping or other means discussed below
allows the pixel size to be determined from the data. Once done, the detector axes may be
specified in a similar manner as to file conversion without a DM file, discussed above. This is
dome by specifying the ``detXalu`` and ``detYalu`` arguments. For example, to set the y-axis
as pixels, and the x-axis in nm:

.. code-block:: python

    >>> import numpy as np
    >>> mb = MerlinBinary(
    ...         binfns='medipixfile.mib', hdrfn='medipixfile.hdr', dmfns=['dmfile.dm3'],
    ...         detYalu=(256, 'y-axis', 'pixels'), detXalu=(np.arange(256)*0.1, 'x-axis', 'nm'))


Without any scan information
============================
If no scan information is passed, and a single binary file is provided, all
images in the file are processed as though the shape was [1, nimages, detY, detX]
(as are TEM Data, discussed below). In this case, `row_end_skip` should be set to 0
unless there are a known number of unwanted images at the end of the dataset. For
example:

.. code-block:: python
    >>> mb = MerlinBinary(
    ...         binfns='medipixfile.mib', hdrfn='medipixfile.hdr', dmfns=[],
    ...         row_end_skip=0)


Flyback
=======
Depending on how the Merlin system is triggered, when acquiring data a number of
extra pixels can be included at the end of each row due to triggering whilst
the beam is moving to the start of the next row of the scan, the `flyback`.
These extra pixels can be removed using the ``row_end_skip`` argument, which is
by default is set to ``1``. Faster acquisitions and larger flyback times will
produce more extra pixels. To use a different value:

.. code-block:: python

    >>> mb = MerlinBinary(
    ...         binfns='medipixfile.mib', hdrfn='medipixfile.hdr', dmfns=['dmfile.dm3'],
    ...         row_end_skip=2)

The number of extra pixels at the end of the data after taking account of flyback
pixels is counted and printed at the end of file conversion. If this is unusually
high (there are often a couple of extra pixels), the ``row_end_skip`` parameter
may be incorrect.



Accessing Files Pre-Conversion
==============================
Conversation to HDF5 is the most flexible solution, however, it is often useful to
access a file before spending the several minutes it takes to convert the data to
HDF5. This is particularly useful when quickly checking a dataset after acquisition
or preparing a mask for use in removal of bad pixels.

The :py:class:`~fpd.fpd_file.MerlinBinary` class provides a number of methods to
access the data in this way. The most useful for common data acquisition modes is the
:py:method:`~fpd.fpd_file.MerlinBinary.get_memmap` method.

.. code-block:: python

    >>> mb = MerlinBinary(
    ...         binfns='medipixfile.mib', hdrfn='medipixfile.hdr', dmfns=['dmfile.dm3'],
    ...         row_end_skip=2)

    >>> mm = mb.get_memmap()

`mm` is a memory mapped file with an array interface. It can be indexed in the usual way,
passed to the processing functions in the :py:mod:`~fpd.fpd_processing` module (or elsewhere)
, or plotted using the :py:class:`~fpd.fpd_file.DataBrowser`. For example:

.. code-block:: python
    >>> from fpd.fpd_file import DataBrowser

    >>> db = DataBrowser(mm, nav_im=np.zeros(mb.shape[:2]))

Where `nav_im` specifies a navigation image, and is set here to be blank for speed. The
memory mapped data could be processed to generate one and will do so automatically if
`nav_im` is not specified. If this navigation image is to be reused, it is best to assign it
to a reusable variable (i.e. nav_im = ...).

Memory mapping only works for certain datasets, and the class will issue appropriate errors
when a file or files cannot be memory mapped. A much slower but more general method is
accessing the data through the array interface of the :py:class:`~fpd.fpd_file.MerlinBinary` class.
For example:

.. code-block:: python

    >>> mb = MerlinBinary(
    ...         binfns='medipixfile.mib', hdrfn='medipixfile.hdr', dmfns=['dmfile.dm3'],
    ...         row_end_skip=2)

    >>> d = mb[0, :10]

The data is in-memory, so care must be taken over the amount of the available system memory
that is used.


Conversion to HDF5
==================
The data may be converted to hdf5 with the :py:method:`~fpd.fpd_file.MerlinBinary.write_hdf5` method.


Data Chunking
-------------
An important parameter is data chunking, which defines how the data is
structured in the HDF5 file. This can have a large influence on the I/O
performance and compression. The default chunking is to store the dataset in cubes
of 16x16[x1]x16x16 pixel chunks. Different settings may be more appropriate for other uses.
The chunking can be set by ``scan_chunks``, ``im_chunks``, or ``chunks``, with
``chunks`` taking precedence over the other chunk parameters. For example:

.. code-block:: python

    >>> mb = MerlinBinary(
    ...         binfns='medipixfile.mib', hdrfn='medipixfile.hdr', dmfns=['dmfile.dm3'])
    >>> mb.write_hdf5(chunks=(8, 8, 256, 256))

The data is repacked by default (``repack=True``), unless memory mapping the file is possible.
Repacking stores the fpd data in a temporary file, then copies it chunk-by-chunk to the final
file. It therefore requires more disk space during conversion, but it is RAM efficient. The
final file sizes are typically smaller and processing the data is faster. If memory mapping
the file is possible, this will automatically happen for writing the data in chunks, avoiding
the intermediate file.


Data Masking
------------
There are sometimes bad pixels (dead or hot or noisy or contaminated ...) on a detector
and these can be interpolated out during conversion by specifying a mask. In the following
example, we generate a very simple mask using memory mapped file access to a specific
image (it could be a flat field) and apply during the conversion. In general, the mask
generation procedure may be more complex.

.. code-block:: python

    >>> mb = MerlinBinary(
    ...         binfns='medipixfile.mib', hdrfn='medipixfile.hdr', dmfns=['dmfile.dm3'])
    >>> mm = mb.get_memmap()
    >>> mask = mm[23, 45] < 6
    >>> mb.write_hdf5(mask=mask)

Locations where the mask is True are replaced using cubic interpolation of the non-masked values.
The mask is stored in the HDF5 file in the usual EMD format.


TEM Data
--------
CTEM data can be processed by setting the y-axis size to 1, and the x-axis size
to the number of images. In this case, ``ds_start_skip`` and ``row_end_skip``
should be set to 0. For example:

.. code-block:: python

    >>> mb = MerlinBinary(
    ...         binfns='medipixfile.mib', hdrfn='medipixfile.hdr', dmfns=['dmfile.dm3'],
    ...         row_end_skip=0, ds_start_skip=0, scanYalu=(1, 'na', 'na'),
    ...         scanXalu=(16, 'Time', 'seconds'))

If the scanXalu axis is None, it is automatically generated from the header
file with axis being the index. Note that the header file contains the
maximum number of frames and the real number of frames could be smaller by
design or error. For example:

.. code-block:: python

    >>> mb = MerlinBinary(
    ...         binfns='medipixfile.mib', hdrfn='medipixfile.hdr', dmfns=['dmfile.dm3'],
    ...         row_end_skip=0, ds_start_skip=0, scanYalu=(1, 'na', 'na'),
    ...         scanXalu=(None, 'images', 'index'))

Here, the y-axis is singular, which replicates the way in which the binary
data is stored.


File Contents
-------------
The Merlin images and metadata, basic analysis of the images, and any DM files
are parsed and embedded in a single HDF5 file in multiple EMD datasets and can
be accessed and viewed in any HDF5 compatible software.

Various processing functions are contained in the :py:mod:`~fpd.fpd_processing`
module.

To be continued...




Conversion to other formats
===========================
Various conversion utilities are included the :py:mod:`~fpd.fpd_file` module.


HyperSpy Signals
----------------
All EMD datasets in an HDF5 file can be converted to 'Lazy' Hyperspy signals
using :py:func:`~fpd.fpd_file.fpd_to_hysperspy`:

.. code-block:: python

    >>> from fpd.fpd_file import fpd_to_hysperspy
    >>> fpd_signals = fpd_to_hysperspy('file.hdf5')
    >>> im = fpd_signals.fpd_data

Here, ``fpd_signals`` is a named tuple containing all recognised EMD datasets
as named fields.


FPD data to binary
------------------
:py:func:`~fpd.fpd_file.hdf5_fpd_to_bin`

To be continued...



Others
------
To be continued...

:py:func:`~fpd.fpd_file.find_hdf5_objname_by_attribute`
:py:func:`~fpd.fpd_file.hdf5_dm_tags_to_dict`
:py:func:`~fpd.fpd_file.hdf5_dm_to_bin`
:py:func:`~fpd.fpd_file.hdf5_src_to_file`



Visualisation
=============

Built-in: :py:class:`~fpd.fpd_file.DataBrowser`

HyperSpy: 

To be continued...


