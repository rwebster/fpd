.. fpd documentation master file, created by
   sphinx-quickstart on Mon Jan 22 11:16:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fpd's documentation!
===============================

A python package for fast pixelated detector data storage, analysis and visualisation.


News
----
24 March 2019 - Release 0.1.6: improvements to :py:class:`~fpd.fpd_file.MerlinBinary` class, speed optimisations in :py:func:`~fpd.fpd_processing.synthetic_images`, 'Uniform' colourmap renamed to 'MLP', additional conversion functions in :py:mod:`~fpd.mag_tools`, and improvements to the single image SNR function, :py:func:`~fpd.utils.snr_single_image`.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   install
   convert_merlin_binary
   related
   news
   fpd


fpd-demos overview
------------------
Many examples of application of the :py:mod:`~fpd` package are included in the fpd-demos repository: 
https://gitlab.com/fpdpy/fpd-demos 

.. raw:: html
    
    <iframe src="https://fpdpy.gitlab.io/fpd-demos/01_fpd_overview.html" height="650px" width="110%"></iframe>
    

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

