.. _install:

**********
Installing
**********

The package is is actively developed and new versions are released to the python
package repository: https://pypi.python.org/pypi/fpd. The source is hosted on 
gitlab: https://gitlab.com/fpdpy/fpd for both the development (master branch)
and released (release branch) versions.

Requirements
============
Python versions 2.7 and 3.x are currently supported. The fpd package depends upon the
following packages:

* numpy
* scipy
* scikit-image
* matplotlib
* h5py
* tqdm
* hyperspy

Most of these are included as standard in the majority of distributions or can be
installed through `pip`, either manually or automatically when installing the fpd
package. 

Hyperspy has many useful features and in this package it is mainly used for reading
Digital Micrograph files. Install instructions can be found at:
http://hyperspy.org/hyperspy-doc/current/user_guide/install.html
Most of the fpd package can be used without Hyperspy being installed. To do this,
simply install all the package dependences manually using your distribution tool or 
pip, and ignore them when installing the fpd package by adding ``--no-deps`` to the
install command (detailed below).

Installation
============

Release version
---------------
The easiest way to install the package on any platform is to use pip (or pip3):

.. code-block:: bash

    pip3 install --user fpd

The ``--user`` option installs the package to your local folder, avoiding any file
permission issues.

Development Version
-------------------
To install from source, download or clone the master (development) version
from the repository and, in the package directory, run:

.. code-block:: bash

    pip3 install --user .

To clone the repository, open a terminal and run:

.. code-block:: bash

    git clone git@gitlab.com:fpdpy/fpd.git

An archive can be downloaded from: https://gitlab.com/fpdpy/fpd/repository/master/archive.zip.

Upgrading and Uninstalling
--------------------------
In all of the above install commands, ``-U`` can be added to force an upgrade 
/ reinstall. When used in combination with ``--no-deps``, only the fpd 
package will be reinstalled.

The package can be removed with:

.. code-block:: bash

    pip3 uninstall fpd

