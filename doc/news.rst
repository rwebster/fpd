.. _news:

************
News Archive
************
24 March 2019 - Release 0.1.6: improvements to :py:class:`~fpd.fpd_file.MerlinBinary` class, speed optimisations in :py:func:`~fpd.fpd_processing.synthetic_images`, 'Uniform' colourmap renamed to 'MLP', additional conversion functions in :py:mod:`~fpd.mag_tools`, and improvements to the single image SNR function, :py:func:`~fpd.utils.snr_single_image`.

18 February 2019 - Release 0.1.5 with improvements to or new lattice functions (:py:func:`~fpd.tem_tools.lattice_from_inliers`, :py:func:`~fpd.tem_tools.blob_log_detect`, :py:func:`~fpd.tem_tools.lattice_resolver`), new SNR and gun noise correction tools in a new :py:mod:`~fpd.utils` module, and many other improvements.

27 October 2018 - Release 0.1.4 with new :py:class:`~fpd.fpd_file.CubicImageInterpolator` and :py:class:`~fpd.fpd_processing.VirtualAnnularImages` classes, several lattice finding (:py:func:`~fpd.tem_tools.blob_log_detect`, :py:func:`~fpd.tem_tools.friedel_filter`, :py:func:`~fpd.tem_tools.synthetic_lattice`, :py:func:`~fpd.tem_tools.vector_combinations`, :py:func:`~fpd.tem_tools.lattice_angles`, :py:func:`~fpd.tem_tools.lattice_magnitudes`, :py:func:`~fpd.tem_tools.lattice_inlier`, :py:func:`~fpd.tem_tools.optimise_lattice`), ctf (:py:func:`~fpd.tem_tools.ctf`, :py:func:`~fpd.tem_tools.scherzer_defocus`, :py:func:`~fpd.tem_tools.defocus_from_ctf_crossing`) and image alignment (:py:func:`~fpd.tem_tools.orb_trans`, :py:func:`~fpd.tem_tools.optimise_trans`, :py:func:`~fpd.tem_tools.apply_image_trans`) functions in the :py:mod:`~fpd.tem_tools` module, and many other improvements. Examples are given in the notebook overview: https://gitlab.com/fpdpy/fpd-demos. 

31 July 2018 - Release 0.1.3 with new :py:class:`~fpd.fpd_file.MerlinBinary` class and other improvements.

25 Mar 2018 - Notebook demos are now available at https://gitlab.com/fpdpy/fpd-demos


