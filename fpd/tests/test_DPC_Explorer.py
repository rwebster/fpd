from __future__ import print_function
import matplotlib.pyplot as plt
import unittest
import numpy as np
import tempfile
import shutil

import fpd
import fpd.fpd_processing as fpdp
import fpd.fpd_file as fpdf


class TestDPC_Explorer(unittest.TestCase):   
    @classmethod
    def setUpClass(self):
        self.tmp_d = tempfile.mkdtemp(prefix='fpd-')
        
    def test_DPC_Explorer_plot(self):
        fpd.DPC_Explorer(64)
        plt.close('all')
        
    def test_DPC_Explorer_plot_save(self):       
        b = fpd.DPC_Explorer(64)
        b.save(self.tmp_d)
        plt.close('all')
    
    def test_DPC_Explorer_ransac(self):
        b = fpd.DPC_Explorer(-64, ransac=True,
                                     ransac_dict={'residual_threshold': 5.0})
        plt.close('all')
        
        noise = 10/2.0
        self.assertTrue(np.all(b.x <= noise+2))
        self.assertTrue(np.all(b.y <= noise+2))
    
    def test_DPC_Explorer_ransac(self):
        b = fpd.DPC_Explorer(-64, yx_range_from_r=True)
        plt.close('all')
        
        b = fpd.DPC_Explorer(-64, yx_range_from_r=False)
        plt.close('all')
    
    def test_DPC_Explorer_median(self):
        b = fpd.DPC_Explorer(-64, median=True)
        plt.close('all')
        
        b = fpd.DPC_Explorer(-64, median=True, median_mode=1)
        plt.close('all') 
    
    def test_DPC_Explorer_seq(self):
        yx = np.random.rand(2, 2, 64, 64)
        b = fpd.DPC_Explorer(d=yx)
        for i in range(4):
            b._on_seq_next(None)
        plt.close('all')
    
    def test_DPC_Explorer_seq_gets(self):
        yx = np.random.rand(3, 2, 64, 64)
        yx -= yx.mean((-2, -1))[..., None, None]

        b = fpd.DPC_Explorer(d=yx)
        b._on_close(None)

        ims = b.get_image_sequence('y')
        ims_multi = b.get_image_sequence(['y', 'x'])
        ims_8bit = b.get_image_sequence('y', True)
        ims_multi_8bit = b.get_image_sequence(['y', 'x'], True)
    
    def test_DPC_Explorer_seq_gets_non_seq(self):
        yx = np.random.rand(2, 64, 64)
        yx -= yx.mean((-2, -1))[..., None, None]

        b = fpd.DPC_Explorer(d=yx)
        b._on_close(None)

        ims = b.get_image_sequence('y')

    @classmethod
    def tearDownClass(self):
        shutil.rmtree(self.tmp_d)
        
        
if __name__ == '__main__':
    unittest.main()


