from __future__ import print_function
import unittest
import numpy as np
import matplotlib.pylab as plt

import fpd
import fpd.fpd_processing as fpdp
import fpd.fpd_file as fpdf


class TestFPDP(unittest.TestCase):
    def setUp(self):
        self.ims = np.random.rand(2, 3, *(256,)*2) - 0.5
    
    def test_BandpassPlotter_1(self):
        bp = fpd.fft_tools.BandpassPlotter(self.ims[0,0], (0.053, 0.71), gy=2, fact=0.75, cmap='viridis')
        plt.close('all')
    
    def test_BandpassPlotter_2(self):
        ims_pass, (im_fft, mask, extent) = fpd.fft_tools.bandpass(self.ims, (0.053, 0.71), gy=2)
        plt.close('all')
    
    def test_BandpassPlotter_3(self):
        ims_pass, (im_fft, mask, extent) = fpd.fft_tools.bandpass(self.ims, (0.053, 0.71), gy=2, mode='horiz')
        plt.close('all')
    
    def test_BandpassPlotter_4(self):
        ims_pass, (im_fft, mask, extent) = fpd.fft_tools.bandpass(self.ims, (0.053, 0.71), gy=2, mode='vert')
        plt.close('all')
    
if __name__ == '__main__':
    unittest.main()


