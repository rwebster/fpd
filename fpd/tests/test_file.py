from __future__ import print_function
import unittest
import os
import zipfile
import tarfile
import tempfile
import shutil
import matplotlib.pylab as plt
plt.ion()
import numpy as np
import h5py

from fpd.fpd_file import MerlinBinary, DataBrowser
import fpd.fpd_file as fpdf


class TestFile(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.tmp_d = tempfile.mkdtemp(prefix='fpd-')
        self.bp = os.path.dirname(__file__)
        
        # multiple images per file
        z_fn = os.path.join(self.bp, 'file_datasets', 'medipix_test_binary_data.zip')
        with zipfile.ZipFile(z_fn, 'r') as z:
            z.extractall(path=self.tmp_d)
            z_fns = z.namelist()
        self.z_dm_fns = [x for x in z_fns if x.split('.')[-1].lower() in ['dm3', 'dm4']]
        self.z_bn_fns = [x for x in z_fns if x.split('.')[-1].lower() in ['bin', 'mib']]
        self.z_hd_fns = [x for x in z_fns if x.split('.')[-1].lower() in ['hdr']]
        self.z_dm_fns.sort()
        self.z_bn_fns.sort()
        self.z_hd_fns.sort()
        
        # multiple single-image files
        self.tmp_d_multi_ims = tempfile.mkdtemp(dir=self.tmp_d, prefix='multiple_single_images-')
        z_fn = os.path.join(self.bp, 'file_datasets', 'zero_values_6bit_multiple_files_16x16.zip')
        with zipfile.ZipFile(z_fn, 'r') as z:
            z.extractall(path=self.tmp_d_multi_ims)
            z_multi_ims_fns = z.namelist()
        self.z_mi_dm_fns = [x for x in z_multi_ims_fns if x.split('.')[-1].lower() in ['dm3', 'dm4']]
        self.z_mi_bn_fns = [x for x in z_multi_ims_fns if x.split('.')[-1].lower() in ['bin', 'mib']]
        self.z_mi_hd_fns = [x for x in z_multi_ims_fns if x.split('.')[-1].lower() in ['hdr']]
        
        # raw mode
        self.tmp_d_raw = tempfile.mkdtemp(dir=self.tmp_d, prefix='raw-')
        z_fn = os.path.join(self.bp, 'file_datasets', 'medipix_test_binary_data_raw.zip')
        with zipfile.ZipFile(z_fn, 'r') as z:
            z.extractall(path=self.tmp_d_raw)
            z_fns = z.namelist()
        self.z_dm_fns_raw = [x for x in z_fns if x.split('.')[-1].lower() in ['dm3', 'dm4']]
        self.z_bn_fns_raw = [x for x in z_fns if x.split('.')[-1].lower() in ['bin', 'mib']]
        self.z_hd_fns_raw = [x for x in z_fns if x.split('.')[-1].lower() in ['hdr']]
        self.z_dm_fns_raw.sort()
        self.z_bn_fns_raw.sort()
        self.z_hd_fns_raw.sort()
    
        # raw and non-raw image data (non-blank)
        self.tmp_d_images = tempfile.mkdtemp(dir=self.tmp_d, prefix='image-')
        z_fn = os.path.join(self.bp, 'file_datasets', 'medipix_test_binary_data_images.zip')
        with zipfile.ZipFile(z_fn, 'r') as z:
            z.extractall(path=self.tmp_d_images)
            z_fns = z.namelist()
        z_fns = [x for x in z_fns if x.startswith('00')]
        
        self.z_dm_fns_ims = [x for x in z_fns if x.split('.')[-1].lower() in ['dm3', 'dm4']]
        self.z_bn_fns_ims = [x for x in z_fns if x.split('.')[-1].lower() in ['bin', 'mib']]
        self.z_hd_fns_ims = [x for x in z_fns if x.split('.')[-1].lower() in ['hdr']]
        self.z_dm_fns_ims.sort()
        self.z_bn_fns_ims.sort()
        self.z_hd_fns_ims.sort()
        
        self.ref_ims = np.load(os.path.join(self.bp, 'file_datasets',
                                            'medipix_test_binary_data_images_fpd_sum_dif_ims.npz'))
        
    def convert_file(self, i, cpd={}, h5pd={}):
        #print(self.z_bn_fns[i])
        dmfns = [os.path.join(self.tmp_d, self.z_dm_fns[i])] # single file
        binfns = os.path.join(self.tmp_d, self.z_bn_fns[i])
        hdrfn = os.path.join(self.tmp_d, self.z_hd_fns[i])
        
        mb = MerlinBinary(binfns, hdrfn, dmfns,
                       ds_start_skip=0, row_end_skip=1, 
                       **cpd)
        fn = mb.write_hdf5(ow=True, **h5pd)
    
    def test_convert(self):
        for i in range(len(self.z_bn_fns)):
            self.convert_file(i)
    
    def convert_file_raw(self, i, cpd={}, h5pd={}):
        #print(self.z_bn_fns[i])
        dmfns = [os.path.join(self.tmp_d_raw, self.z_dm_fns_raw[i])]
        binfns = os.path.join(self.tmp_d_raw, self.z_bn_fns_raw[i])
        hdrfn = os.path.join(self.tmp_d_raw, self.z_hd_fns_raw[i])
        
        mb = MerlinBinary(binfns, hdrfn, dmfns,
                       ds_start_skip=0, row_end_skip=1, 
                       **cpd)
        fn = mb.write_hdf5(ow=True, **h5pd)
    
    def test_convert_raw(self):
        for i in range(len(self.z_bn_fns_raw)):
            self.convert_file_raw(i)    
    
    def test_convert_mi(self):
        # multiple images
        dmfns = [os.path.join(self.tmp_d_multi_ims, self.z_mi_dm_fns[0])]
        binfns = [os.path.join(self.tmp_d_multi_ims, t) for t in self.z_mi_bn_fns]
        hdrfn = os.path.join(self.tmp_d_multi_ims, self.z_mi_hd_fns[0])
        
        mb = MerlinBinary(binfns, hdrfn, dmfns,
                       ds_start_skip=0, row_end_skip=1)
        fn = mb.write_hdf5(ow=True)
    
    def test_convert_images(self):
        # files with images
        for i in range(len(self.z_dm_fns_ims)):
            tag = self.z_dm_fns_ims[i].split('.')[0]
            
            dmfns = [os.path.join(self.tmp_d_images, self.z_dm_fns_ims[i])]
            binfns = os.path.join(self.tmp_d_images, self.z_bn_fns_ims[i])
            hdrfn = os.path.join(self.tmp_d_images, self.z_hd_fns_ims[i])
            
            mb = MerlinBinary(binfns, hdrfn, dmfns,
                        ds_start_skip=0, row_end_skip=1)
            fn = mb.write_hdf5(ow=True)

            ref_im = self.ref_ims[tag]
            #plt.matshow(ref_im)
            with h5py.File(os.path.join(self.tmp_d_images, tag + '.hdf5')) as h:
                new_im = h['fpd_expt/fpd_sum_dif/data'][:]
            assert (np.isclose(np.abs(new_im-ref_im).sum(), 0))
            # isclose used in case of dtype issues
    
    def test_to_array(self):
        i = 2 # SPM mode
        dmfns = [os.path.join(self.tmp_d, self.z_dm_fns[i])]
        binfns = os.path.join(self.tmp_d, self.z_bn_fns[i])
        hdrfn = os.path.join(self.tmp_d, self.z_hd_fns[i])
        
        mb = MerlinBinary(binfns, hdrfn, dmfns,
                            ds_start_skip=0, row_end_skip=1)
        print('to array')
        a = mb.to_array()
        
    def test_to_array_max(self):
        i = 2 # SPM mode
        dmfns = [os.path.join(self.tmp_d, self.z_dm_fns[i])]
        binfns = os.path.join(self.tmp_d, self.z_bn_fns[i])
        hdrfn = os.path.join(self.tmp_d, self.z_hd_fns[i])
        
        mb = MerlinBinary(binfns, hdrfn, dmfns,
                            ds_start_skip=0, row_end_skip=1)
        print('to array (read_max)')
        a = mb.to_array(read_max=mb._scanX)
    
    def test_getitem(self):
        i = 2 # SPM mode
        dmfns = [os.path.join(self.tmp_d, self.z_dm_fns[i])]
        binfns = os.path.join(self.tmp_d, self.z_bn_fns[i])
        hdrfn = os.path.join(self.tmp_d, self.z_hd_fns[i])
        
        mb = MerlinBinary(binfns, hdrfn, dmfns,
                            ds_start_skip=0, row_end_skip=1)
        print('getitem')
        a = mb[:]
        a = mb[...]
        a = mb[:, ...]
        a = mb[..., :, 0]
        a = mb[0, 0, 0, 0]
        a = mb[::2, ::1, ..., ::4, ::4]
        del a
        del mb
    
    def test_get_memmap(self):
        i = 2 # SPM mode
        dmfns = [os.path.join(self.tmp_d, self.z_dm_fns[i])]
        binfns = os.path.join(self.tmp_d, self.z_bn_fns[i])
        hdrfn = os.path.join(self.tmp_d, self.z_hd_fns[i])
        
        mb = MerlinBinary(binfns, hdrfn, dmfns,
                            ds_start_skip=0, row_end_skip=1)
        print('getmemmap')
        mm = mb.get_memmap()
        a = mm.sum((-2, -1))
        a = mm[:]
        a = mm[...]
        a = mm[:, ...]
        a = mm[..., :, 0]
        a = mm[0, 0, 0, 0]
        a = mm[::2, ::1, ..., ::4, ::4]
        del a, mm, mb    
    
    def test_file_browser_ndarray(self):
        data = np.random.rand( *(16,)*4 )
        d = DataBrowser(data)
        plt.close('all')
    
    def test_file_browser_ndarray_nav_im(self):
        data = np.random.rand( *(16,)*4 )
        mav_im = data.sum((-2, -1))
        d = DataBrowser(data, nav_im=mav_im)
        plt.close('all')
    
    def test_zForLast_file_browser(self):
        # run last for headless closing of file issues
        i = 2 # SPM mode
           
        binfn = os.path.splitext(self.z_bn_fns[i])[0]+'.hdf5'
        h5fn = os.path.join(self.tmp_d, binfn)
        if not os.path.isfile(h5fn):
            self.convert_file(i)
        d = DataBrowser(h5fn, fpd_check=True)
        #d.h5f.file.close()  # for headless plotting
        plt.close('all')
    
    def test_zForLast_file_browser_kwd_dict(self):
        # run last for headless closing of file issues
        i = 2 # SPM mode
           
        binfn = os.path.splitext(self.z_bn_fns[i])[0]+'.hdf5'
        h5fn = os.path.join(self.tmp_d, binfn)
        if not os.path.isfile(h5fn):
            self.convert_file(i)
        d = DataBrowser(h5fn, fpd_check=True, nav_im_dict={'cmap': 'jet', 'vmin': 0, 'vmax': 1})
        #d.h5f.file.close()  # for headless plotting
        plt.close('all')
    
    def test_scanYalu(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'scanYalu.hdf5')
        scanYalu = (8, 'y-axis', 'nm')
        self.convert_file(i, h5pd={'h5fn':h5fn}, cpd={'scanYalu':scanYalu})
        
    def test_scanXalu(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'scanXalu.hdf5')
        scanXalu = (16, 'x-axis', 'nm')
        self.convert_file(i, h5pd={'h5fn':h5fn}, cpd={'scanXalu':scanXalu})
    
    def test_scanYXalu(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'scanYXalu.hdf5')
        scanYalu = (8, 'y-axis', 'nm')
        scanXalu = (16, 'x-axis', 'nm')
        self.convert_file(i, h5pd={'h5fn':h5fn}, cpd={'scanYalu':scanYalu, 'scanXalu':scanXalu})
    
    def test_scanYalu_array(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'scanYalu_array.hdf5')
        scanYalu = (np.arange(8)*0.1, 'y-axis', 'nm')
        self.convert_file(i, h5pd={'h5fn':h5fn}, cpd={'scanYalu':scanYalu})
    
    def test_scanXalu_array(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'scanXalu_array.hdf5')
        scanXalu = (np.arange(16)*0.1, 'y-axis', 'nm')
        self.convert_file(i, h5pd={'h5fn':h5fn}, cpd={'scanXalu':scanXalu})
    
    
    
    def test_detYalu(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'detYalu.hdf5')
        detYalu = (256, 'y-axis', 'nm')
        self.convert_file(i, h5pd={'h5fn':h5fn}, cpd={'detYalu':detYalu})
        
    def test_detXalu(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'detXalu.hdf5')
        detXalu = (256, 'x-axis', 'nm')
        self.convert_file(i, h5pd={'h5fn':h5fn}, cpd={'detXalu':detXalu})
    
    def test_detYXalu(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'detYXalu.hdf5')
        detYalu = (256, 'y-axis', 'nm')
        detXalu = (256, 'x-axis', 'nm')
        self.convert_file(i, h5pd={'h5fn':h5fn}, cpd={'detYalu':detYalu, 'detXalu':detXalu})
    
    def test_detYalu_array(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'detYalu_array.hdf5')
        detYalu = (np.arange(256)*0.1, 'y-axis', 'nm')
        self.convert_file(i, h5pd={'h5fn':h5fn}, cpd={'detYalu':detYalu})
    
    def test_detXalu_array(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'detXalu_array.hdf5')
        detXalu = (np.arange(256)*0.1, 'y-axis', 'nm')
        self.convert_file(i, h5pd={'h5fn':h5fn}, cpd={'detXalu':detXalu})
    
    
    
    def test_compression_opts_setting(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'compression.hdf5')
        self.convert_file(i, h5pd={'h5fn':h5fn, 'compression_opts':0})
    
    def test_mask(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'masked.hdf5')
        mask = np.eye(256,256)
        self.convert_file(i, h5pd={'h5fn':h5fn, 'mask':mask})
        
    def test_chunks_setting(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'chunks.hdf5')
        self.convert_file(i, h5pd={'h5fn':h5fn, 'chunks':(1,1,128,128)})
        
    def test_no_repacking(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'repacked.hdf5')
        self.convert_file(i, h5pd={'h5fn':h5fn, 'chunks':(8,)*4, 'repack':False})
    
    def test_chunk_coercion(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'repacked.hdf5')
        self.convert_file(i, h5pd={'h5fn':h5fn, 'chunks':(16,)*4, 'repack':True})
    
    def test_im_chunks_setting(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'im_chunks.hdf5')
        self.convert_file(i, h5pd={'h5fn':h5fn, 'im_chunks':(32, 32)})
    
    def test_scan_chunks_setting(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'scan_chunks.hdf5')
        self.convert_file(i, h5pd={'h5fn':h5fn, 'scan_chunks':(2, 2)})
    
    def test_convert_all(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'convert_all.hdf5')
        dmfns = []
        binfns = os.path.join(self.tmp_d, self.z_bn_fns[i])
        hdrfn = os.path.join(self.tmp_d, self.z_hd_fns[i])
        
        mb = MerlinBinary(binfns, hdrfn, dmfns,
                          ds_start_skip=0, row_end_skip=0)
        fn = mb.write_hdf5(ow=True, h5fn=h5fn)
    
    def test_no_memmap(self):
        i = 2 # SPM mode
        h5fn = os.path.join(self.tmp_d, 'convert_all_no_memmap.hdf5')
        dmfns = []
        binfns = os.path.join(self.tmp_d, self.z_bn_fns[i])
        hdrfn = os.path.join(self.tmp_d, self.z_hd_fns[i])
        
        mb = MerlinBinary(binfns, hdrfn, dmfns,
                          ds_start_skip=0, row_end_skip=0)
        fn = mb.write_hdf5(ow=True, h5fn=h5fn, allow_memmap=False)
    
    def test_tools(self):
        i = 2 # SPM mode
           
        binfn = os.path.splitext(self.z_bn_fns[i])[0]+'.hdf5'
        h5fn = os.path.join(self.tmp_d, binfn)
        # reconvert with source embedded
        self.convert_file(i, h5pd={'embed_source':True})
        
        fpg = h5fn
        b, vs = fpdf._check_fpd_file(fpg, min_version=None, max_version=None)
        
        tagds, dm_group_paths, dm_filenames = fpdf.hdf5_dm_tags_to_dict(fpg, 
                                                                        fpd_check=True)
        
        out_dm_filenames = [os.path.join(self.tmp_d, t.split('.')[0]+'_extracted') for t in dm_filenames]
        fpdf.hdf5_dm_to_bin(fpg, dmfns=out_dm_filenames, fpd_check=True, ow=True)
        
        fpdf.hdf5_fpd_to_bin(fpg, fpd_fn=os.path.join(self.tmp_d, 'bin_extract.bin'),
                             fpd_check=True, ow=False)
        
        fpdf.hdf5_src_to_file(fpg, src_fn=os.path.join(self.tmp_d, 'fpd_file.py'),
                              fpd_check=True, ow=False)
        
        fpd_signals = fpdf.fpd_to_hysperspy(fpg, fpd_check=True, assume_yes=True)
        
        fpd_signals = fpdf.fpd_to_hysperspy(fpg, fpd_check=True, assume_yes=True,
                                            group_names=['fpd_data', 
                                                         'fpd_sum_im',
                                                         'fpd_sum_dif'])
    
    def test_v0p67p0p8(self):
        # 1d scan of 16 images, no dm file
        
        z_fn = os.path.join(self.bp, 'file_datasets', 'medipix_test_binary_data_v0p67p0p8_small_16.tar.gz')
        
        with tarfile.open(z_fn, "r:gz") as tar:
            names = tar.getnames()
            path = names[0]
            tar.extractall(path=self.tmp_d)
        
        mibfiles = [n for n in names if n.endswith('.mib')]
        hdrfiles = [n for n in names if n.endswith('.hdr')]
        mibfiles.sort()
        hdrfiles.sort()
        
        binfns = os.path.join(self.tmp_d, mibfiles[0])
        hdrfn = os.path.join(self.tmp_d, hdrfiles[0])
        dmfns = []
        
        scanYalu = (1, 'na', 'na')
        scanXalu = (None, 'images', 'index')
        
        mb = MerlinBinary(binfns, hdrfn, dmfns,
                       ds_start_skip=0, row_end_skip=0, 
                       scanYalu=scanYalu, scanXalu=scanXalu)
        fn = mb.write_hdf5(ow=True)
    
    def test_CubicImageInterpolator(self):
        yi, xi = np.indices((32, 32))
        ri = ((yi-yi.mean())**2 + (xi-xi.mean())**2)**0.5
        image = ri.copy()
        mask = np.logical_and(ri>6, ri<8)
        image[mask] = 0

        #plt.matshow(ri)
        #plt.matshow(image)
        #plt.matshow(mask)

        CI = fpdf.CubicImageInterpolator(mask)
        image_masked = CI.interpolate_image(image, in_place=False)
        #plt.matshow(image_masked)
        #plt.matshow(image)

        assert np.isclose(ri, image_masked, rtol=1e-02, atol=0.5).all()
        assert np.isclose(ri, image, rtol=1e-02, atol=0.5).all() == False

        CI.interpolate_image(image, in_place=True)
        assert np.isclose(ri, image, rtol=1e-02, atol=0.5).all() == True
        #plt.matshow(image)
    
    @classmethod
    def tearDownClass(self):
        #print(self.tmp_d)
        shutil.rmtree(self.tmp_d)
        #pass
    
    
    '''
    # for testing old files
    bd = '/media/Data/ssp-serv/fpd/merlin_test_data/new_merlin_card_20150824/'
    fns =   [u'ccsm010ms.hdr',
            u'ccsm010ms.mib',
            u'cm010ms.hdr',
            u'cm010ms.mib',
            u'csm010ms.hdr',
            u'csm010ms.mib',
            u'spm010ms.hdr',
            u'spm010ms.mib']

    hdr_fns = fns[0::2]
    bin_fns = fns[1::2]


    for h,f in zip(hdr_fns,bin_fns):
        print(os.path.join(bd,f))
        print(os.path.join(bd,h))
        binary_to_hdf5(os.path.join(bd,f), os.path.join(bd,h), ds_start_skip=0,
                    row_end_skip=0, ow=True,
                    scanYalu=(1,0,'y'), scanXalu=(1,0,'x'))

    '''


if __name__ == '__main__':
    unittest.main()
