from __future__ import print_function
import unittest
import numpy as np
import scipy as sp
import matplotlib.pylab as plt
import os

import fpd
import fpd.fpd_processing as fpdp


def close_enough(a, b, rtol, atol):
    dif = np.allclose(a, b, rtol=rtol, atol=atol, equal_nan=False)
    return np.all(dif)


def params_f(image, v):
    return v

def nonuniform_f(image):
    l = np.random.randint(2)+1
    return np.arange(l)


class TestFPDP(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.radius = 32
        sa = fpd.synthetic_data.shift_array(scan_len=9, shift_min=-2.0, 
                                            shift_max=2.0)
        self.sa = np.asarray(sa)
        im = fpd.synthetic_data.disk_image(intensity=128, radius=self.radius,
                                           size=256, upscale=8, dtype='uint8')#float)
        self.data = fpd.synthetic_data.shift_images(self.sa, im, noise=False, origin='bottom')
        self.sdpc_data = fpd.synthetic_data.shift_images(self.sa, im, origin='top')
        
        # error based on 1/100 pixel phase correlation
        self.rtol = 1e-09
        self.atol = 5e-02
        
        # nrmse
        self.nrmse_ref_im = fpd.synthetic_data.disk_image(intensity=64, radius=32, noise=False)
    
    
    def test_segmented_dpc_oct(self):
        detectors = fpd.synthetic_data.segmented_detectors(im_shape=(256, 256),
                                                           rio=(24, 128),
                                                           ac_det_roll=0)
        det_sigs = fpd.synthetic_data.segmented_dpc_signals(self.sdpc_data, 
                                                            detectors)
        
        d = fpd.SegmentedDPC(det_sigs, alpha=self.radius)
        mdyx = [d.mdpc_betay.data, d.mdpc_betax.data]
        dyx = [d.dpc_betay.data, d.dpc_betax.data]

        self.assertTrue(close_enough(self.sa, np.asarray(mdyx), self.rtol, self.atol))
        self.assertTrue(close_enough(self.sa, np.asarray(dyx), self.rtol, self.atol))
    
    def test_segmented_dpc_quad(self):
        detectors = fpd.synthetic_data.segmented_detectors(im_shape=(256, 256),
                                                           rio=(64, 128),
                                                           ac_det_roll=0)
        det_sigs = fpd.synthetic_data.segmented_dpc_signals(self.sdpc_data, 
                                                            detectors)
        det_sigs = det_sigs[:4]
        
        d = fpd.SegmentedDPC(det_sigs, alpha=self.radius)
        dyx = [d.dpc_betay.data, d.dpc_betax.data]
        self.assertTrue(close_enough(self.sa, np.asarray(dyx), self.rtol, self.atol))

    
    def test_com(self):
        out = fpdp.center_of_mass(self.data, nr=None, nc=None, origin='bottom')
        out -= out.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, out, self.rtol, self.atol))
    
    def test_com_threshold(self):
        out = fpdp.center_of_mass(self.data, nr=None, nc=None, origin='bottom', thr='otsu')
        out -= out.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, out, self.rtol, self.atol))
        
    def test_com_nostats(self):
        print('Testing print_stats=False')
        out = fpdp.center_of_mass(self.data, nr=None, nc=None, print_stats=False, origin='bottom')
        out -= out.mean((1,2))[:, None, None]
        print('Done\n')
        self.assertTrue(close_enough(self.sa, out, self.rtol, self.atol))
    
    def test_com_ck(self):
        out = fpdp.center_of_mass(self.data, nr=3, nc=3, origin='bottom')
        out -= out.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, out, self.rtol, self.atol))
    
    def test_com_ck_nrnc_are_chunks(self):
        out = fpdp.center_of_mass(self.data, nr=3, nc=3, origin='bottom', nrnc_are_chunks=True)
        out -= out.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, out, self.rtol, self.atol))
    
    def test_com_ck_rebin(self):
        out = fpdp.center_of_mass(self.data, nr=3, nc=3, rebin=2, origin='bottom')
        out -= out.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, out, self.rtol, self.atol))
    
    def test_com_ck_non_parallel(self):
        out = fpdp.center_of_mass(self.data, nr=3, nc=3, parallel=False, origin='bottom')
        out -= out.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, out, self.rtol, self.atol))
    
    def test_com_ck_parallel_ncores(self):
        out = fpdp.center_of_mass(self.data, nr=3, nc=3, ncores=1, origin='bottom')
        out -= out.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, out, self.rtol, self.atol))
    
    
    
    def test_phase_2d(self):
        r1 = fpdp.phase_correlation(self.data, nr=None, nc=None, cyx=(128,)*2,
                                    crop_r=64, spf=100, origin='bottom')
        shift_yx, shift_err, shift_difp, ref = r1
        shift_yx -= shift_yx.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, shift_yx, self.rtol, self.atol))
    
    def test_phase_2d_clip(self):
        r1 = fpdp.phase_correlation(self.data, nr=None, nc=None, cyx=(128,)*2,
                                    crop_r=64, spf=100, origin='bottom',
                                    der_clip_fraction=0.2, der_clip_max_pct=99)
        shift_yx, shift_err, shift_difp, ref = r1
        shift_yx -= shift_yx.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, shift_yx, self.rtol, self.atol))
    
    
    def test_phase_2d_no_print(self):
        print('Testing print_stats=False')
        r1 = fpdp.phase_correlation(self.data, nr=None, nc=None, cyx=(128,)*2,
                                    crop_r=64, spf=100, print_stats=False, origin='bottom')
        shift_yx, shift_err, shift_difp, ref = r1
        shift_yx -= shift_yx.mean((1,2))[:, None, None]
        print('Done\n')
        self.assertTrue(close_enough(self.sa, shift_yx, self.rtol, self.atol))
        
    def test_phase_2d_ck(self):
        r1 = fpdp.phase_correlation(self.data, nr=3, nc=3, cyx=(128,)*2,
                                    crop_r=64, spf=100, origin='bottom')
        shift_yx, shift_err, shift_difp, ref = r1
        shift_yx -= shift_yx.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, shift_yx, self.rtol, self.atol))
        
    def test_phase_2d_ck_nrnc_are_chunks(self):
        r1 = fpdp.phase_correlation(self.data, nr=3, nc=3, cyx=(128,)*2,
                                    crop_r=64, spf=100, origin='bottom',
                                    nrnc_are_chunks=True)
        shift_yx, shift_err, shift_difp, ref = r1
        shift_yx -= shift_yx.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, shift_yx, self.rtol, self.atol))
    
    def test_phase_2d_ck_rebin(self):
        r1 = fpdp.phase_correlation(self.data, nr=3, nc=3, cyx=(128,)*2,
                                    crop_r=64, spf=100, rebin=2, origin='bottom')
        shift_yx, shift_err, shift_difp, ref = r1
        shift_yx -= shift_yx.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, shift_yx, self.rtol, self.atol))  
    
    def test_phase_2d_ck_non_parallel(self):
        r1 = fpdp.phase_correlation(self.data, nr=3, nc=3, cyx=(128,)*2,
                                    crop_r=64, spf=100, parallel=False, origin='bottom')
        shift_yx, shift_err, shift_difp, ref = r1
        shift_yx -= shift_yx.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, shift_yx, self.rtol, self.atol))
    
    def test_phase_2d_ck_parallel_ncores(self):
        r1 = fpdp.phase_correlation(self.data, nr=3, nc=3, cyx=(128,)*2,
                                    crop_r=64, spf=100, ncores=1, origin='bottom')
        shift_yx, shift_err, shift_difp, ref = r1
        shift_yx -= shift_yx.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, shift_yx, self.rtol, self.atol))    
        
    def test_phase_1d(self):
        r2 = fpdp.phase_correlation(self.data, nr=None, nc=None, cyx=(128,)*2,
                                    crop_r=64, mode='1d', spf=100, origin='bottom')
        shift_yx, shift_err, shift_difp, ref = r2
        shift_yx -= shift_yx.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, shift_yx, self.rtol, self.atol))
        
    def test_phase_1d_ck(self):
        r2 = fpdp.phase_correlation(self.data, nr=3, nc=3, cyx=(128,)*2,
                                    crop_r=64, mode='1d', spf=100, origin='bottom')
        shift_yx, shift_err, shift_difp, ref = r2
        shift_yx -= shift_yx.mean((1,2))[:, None, None]
        self.assertTrue(close_enough(self.sa, shift_yx, self.rtol, self.atol))
    
    
    def test_sum_im(self):
        sum_im = fpdp.sum_im(self.data, nr=3, nc=3)
    
    def test_sum_dif(self):
        dif_im = fpdp.sum_dif(self.data, nr=3, nc=3)
    
    def test_synthetic_images(self):
        aps = fpdp.synthetic_aperture(self.data.shape[-2:], (128,)*2, np.linspace(16, 192, 4))
        synth_ims = fpdp.synthetic_images(self.data, nr=3, nc=3, apertures=aps)
    
    def test_synthetic_images_rebin(self):
        aps = fpdp.synthetic_aperture(self.data.shape[-2:], (128,)*2, np.linspace(16, 192, 4))
        synth_ims = fpdp.synthetic_images(self.data, nr=3, nc=3, apertures=aps, rebin=2)
        
    def test_sum_im_nrnc_are_chunks(self):
        sum_im = fpdp.sum_im(self.data, nr=3, nc=3, nrnc_are_chunks=True)
    
    def test_sum_dif_nrnc_are_chunks(self):
        dif_im = fpdp.sum_dif(self.data, nr=3, nc=3, nrnc_are_chunks=True)
    
    def test_synthetic_images_nrnc_are_chunks(self):
        aps = fpdp.synthetic_aperture(self.data.shape[-2:], (128,)*2, np.linspace(16, 192, 4))
        synth_ims = fpdp.synthetic_images(self.data, nr=3, nc=3, apertures=aps, nrnc_are_chunks=True)
    
    
    
    # nrmse
    def test_nrmse_1image(self):
        rtn = fpdp.nrmse(self.nrmse_ref_im, self.nrmse_ref_im)
        assert (rtn==0).all()
    
    def test_nrmse_1Dimages(self):
        rtn = fpdp.nrmse(self.nrmse_ref_im, self.nrmse_ref_im[None, ...])
        assert (rtn==0).all()
    
    def test_nrmse_1image(self):
        rtn = fpdp.nrmse(self.nrmse_ref_im, self.nrmse_ref_im[None, None, ...])
        assert (rtn==0).all()
    
    
    # matching images
    def test_matching(self):
        from fpd.synthetic_data import disk_image, shift_array, shift_images
        
        # generate synthetic data
        disc = disk_image(radius=32, intensity=64)
        shift_array = shift_array(6, shift_min=-1, shift_max=1)
        
        # set shifts on diagonal to zero 
        diag_inds = [np.diag(x) for x in np.indices(shift_array[0].shape)]
        shift_array[0][diag_inds] = 0
        shift_array[1][diag_inds] = 0
        
        # shift images
        images = shift_images(shift_array, disc, noise=False)
        aperture = fpdp.synthetic_aperture(images.shape[-2:], cyx=(128,)*2, rio=(0, 48), sigma=0, aaf=1)[0]
        
        matching = fpdp.find_matching_images(images, aperture, plot=True)
        assert (matching.ims_best.std(0) == 0).all()
        assert (matching.ims_common.std(0) == 0).all()
        
        # test no plotting
        matching = fpdp.find_matching_images(images, aperture, plot=False)
    
    
    # find_circ_centre
    def test_find_circ_centre(self):
        r = 32
        image = fpd.synthetic_data.disk_image(radius=r, intensity=64, sigma=4)
        image = image + np.random.rand(*image.shape)*4

        mask = fpd.synthetic_data.disk_image(radius=r+5, intensity=5, sigma=0) > 0.5

        cyx, cr = fpdp.find_circ_centre(image, sigma=6, rmms=(2, int(image.shape[0]/2.0), 1), plot=False)
        cyx, cr = fpdp.find_circ_centre(image, sigma=6, rmms=(r-4, r+4, 1), plot=True)
        cyx, cr = fpdp.find_circ_centre(image, sigma=6, rmms=(r-4, r+4, 1), plot=False, pct=90)
        cyx, cr = fpdp.find_circ_centre(image, sigma=6, rmms=(r-4, r+4, 1), plot=False, mask=mask)
        cyx, cr = fpdp.find_circ_centre(image, sigma=6, rmms=(r-4, r+4, 1), plot=False, mask=mask, pct=90)
        cyx, cr = fpdp.find_circ_centre(image, sigma=6, rmms=(r-4, r+4, 1), plot=False, mask=mask, pct=90, spf=2)
        plt.close('all')
        
        # multiple discs
        from fpd.synthetic_data import shift_im
        im2 = fpd.synthetic_data.disk_image(intensity=32, radius=24, sigma=2, size=256, noise=False)
        shifted_im = shift_im(im2, dyx=(64, 64), noise=False)
        im_multidiscs = image + shifted_im
        #plt.matshow(im_multidiscs)
        cyx, cr = fpdp.find_circ_centre(image, sigma=6, rmms=(20, 36, 1), plot=True, max_n=2)
        #print(cyx, cr)
        cyx, cr = fpdp.find_circ_centre(image, sigma=6, rmms=(20, 36, 1), plot=False, max_n=2)
        #print(cyx, cr)
        plt.close('all')
    
    
    # test map_image_function
    def test_map_image_function(self):
        func = sp.ndimage.center_of_mass
        out = fpdp.map_image_function(self.data, nr=None, nc=None, func=func)
        assert out.ndim == 3
        
    def test_map_image_function_params(self):
        val = 2
        out = fpdp.map_image_function(self.data, nr=None, nc=None, func=params_f, params={'v' : val})
        assert np.isclose(val, out).all()
        assert out.ndim == 2
        
    def test_map_image_function_nonuniform(self):
        out = fpdp.map_image_function(self.data, nr=None, nc=None, func=nonuniform_f)
        assert out.ndim == 2
    
    def test_map_image_function_print(self):
        func = sp.ndimage.center_of_mass
        print('Testing print_stats=False')
        out = fpdp.map_image_function(self.data, nr=None, nc=None, print_stats=False, func=func)
        print('Done\n')
    
    def test_map_image_function_ck(self):
        func = sp.ndimage.center_of_mass
        out = fpdp.map_image_function(self.data, nr=3, nc=3, func=func)
    
    def test_map_image_function_ck_nrnc_are_chunks(self):
        func = sp.ndimage.center_of_mass
        out = fpdp.map_image_function(self.data, nr=3, nc=3, func=func, nrnc_are_chunks=True)
    
    def test_map_image_function_ck_rebin(self):
        func = sp.ndimage.center_of_mass
        out = fpdp.map_image_function(self.data, nr=3, nc=3, rebin=2, func=func)
    
    def test_map_image_function_ck_non_parallel(self):
        func = sp.ndimage.center_of_mass
        out = fpdp.map_image_function(self.data, nr=3, nc=3, parallel=False, func=func)
    
    def test_map_image_function_ck_parallel_ncores(self):
        func = sp.ndimage.center_of_mass
        out = fpdp.map_image_function(self.data, nr=3, nc=3, ncores=1, func=func)
    
    def test_VirtualAnnularImages(self):
        V = fpdp.VirtualAnnularImages(self.data, nr=3, nc=3, cyx= (128,)*2)
        
        im = V.annular_slice(10, 20)
        
        V.plot()
        plt.close('all')
        
        V.plot(r1=20, r2=30, norm='log', nav_im=np.ones(self.data.shape[-2:]), alpha=0.2, cmap='gray', pct=1)
        plt.close('all')
        
        V.plot(mradpp=2.0)
        plt.close('all')
        
    def test_VirtualAnnularImages_save_load(self):
        V = fpdp.VirtualAnnularImages(self.data, nr=3, nc=3, cyx= (128,)*2)
        V.save_data('test')
        
        fn = 'test.npz'
        V = fpdp.VirtualAnnularImages(fn)
        V.plot()
        plt.close('all')
        
        d = dict(np.load(fn))
        V = fpdp.VirtualAnnularImages(d)
        V.plot()
        plt.close('all')
        
        os.remove(fn)
    
    # test make_ref_im
    def test_make_ref_im(self):
        image = fpd.synthetic_data.disk_image(radius=32, intensity=64)
        cyx, cr = fpdp.find_circ_centre(image, sigma=6, rmms=(2, int(image.shape[0]/2.0), 1), plot=False)
        edge_sigma = fpdp.disc_edge_sigma(image, sigma=2, cyx=cyx, r=cr, plot=False)[0]
        aperture = fpdp.synthetic_aperture(image.shape[-2:], cyx=cyx, rio=(0, cr+32), sigma=0, aaf=1)[0]
        
        t1 = fpdp.make_ref_im(image, edge_sigma, plot=False)
        t1 = fpdp.make_ref_im(image, edge_sigma, plot=True)
        t2 = fpdp.make_ref_im(image, edge_sigma, aperture, plot=False)
        t3 = fpdp.make_ref_im(image, edge_sigma, aperture, bin_opening=4, bin_closing=4, plot=False)
        t4 = fpdp.make_ref_im(image, edge_sigma, aperture, bin_opening=4, bin_closing=4, crop_pad=True, plot=False)
    
    def test_rot_vector_accuracy(self):
        yx = np.array([0, 1])
        yx_rot_real = np.array([1, 0])
        yx_rot = fpdp.rotate_vector(yx, theta=90, axis=0)
        assert np.allclose(yx_rot_real.flat, yx_rot.flat)
    
    def test_rot_vector_dims(self):
        yx = np.array([0, 1])
        yx_rot = fpdp.rotate_vector(yx, theta=90, axis=0)
        yx_rot = fpdp.rotate_vector(yx[..., None], theta=90, axis=0)
        yx_rot = fpdp.rotate_vector(yx[None, ..., None], theta=90, axis=1)
    
    def test_cpu_thread_lib_check(self):
        multi_thread = fpdp.cpu_thread_lib_check(n=2000)


if __name__ == '__main__':
    unittest.main()


