from __future__ import print_function
import unittest
import numpy as np
import matplotlib.pylab as plt

import fpd
import fpd.fpd_processing as fpdp
import fpd.fpd_file as fpdf


class TestFPDP_disc_edge_sigma(unittest.TestCase):
    def test_hyperspy(self):
        sigma = 5.0
        
        im = fpd.synthetic_data.disk_image(intensity=1024, radius=32, sigma=sigma, size=256, noise=False)
        cyx, r = fpd.fpd_processing.find_circ_centre(im, 2, (22, int(256/2.0), 1), spf=1, plot=False)
        
        returns = fpdp.disc_edge_sigma(im, sigma=6, cyx=cyx, r=r, use_hyperspy=True, plot=False)
        sigma_wt_avg, sigma_wt_std, sigma_std, (sigma_vals, sigma_stds) = returns
        
        self.assertTrue(np.abs(sigma_wt_avg/sigma - 1) <= 0.02)
        
        returns = fpdp.disc_edge_sigma(im, sigma=6, cyx=cyx, r=r, use_hyperspy=True, plot=True)
        plt.close('all')
    
    def test_non_hyperspy(self):
        sigma = 5.0
        
        im = fpd.synthetic_data.disk_image(intensity=1024, radius=32, sigma=sigma, size=256, noise=False)
        cyx, r = fpd.fpd_processing.find_circ_centre(im, 2, (22, int(256/2.0), 1), spf=1, plot=False)
        
        returns = fpdp.disc_edge_sigma(im, sigma=6, cyx=cyx, r=r, use_hyperspy=False, plot=False)
        sigma_wt_avg, sigma_wt_std, sigma_std, (sigma_vals, sigma_stds) = returns
        
        self.assertTrue(np.abs(sigma_wt_avg/sigma - 1) <= 0.02)
        
        returns = fpdp.disc_edge_sigma(im, sigma=6, cyx=cyx, r=r, use_hyperspy=False, plot=True)
        plt.close('all')

if __name__ == '__main__':
    unittest.main()


