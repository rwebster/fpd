from __future__ import print_function
import unittest
import numpy as np
import matplotlib.pylab as plt


from fpd import mag_tools 


class TestMag_tools(unittest.TestCase):   
    def test_landau(self):
        my, mx = mag_tools.landau()
        my, mx = mag_tools.landau(shape=(256,)*2)
        my, mx = mag_tools.landau(plot=True)
        my, mx = mag_tools.landau(origin='top')
        my, mx = mag_tools.landau(origin='bottom')
        my, mx = mag_tools.landau(pad_width=None)
        my, mx = mag_tools.landau(pad_width=50)
        plt.close('all')
    
    def test_stripes(self):
        my, mx = mag_tools.stripes()
        my, mx = mag_tools.stripes(shape=(256,)*2)
        my, mx = mag_tools.stripes(plot=True)
        my, mx = mag_tools.stripes(origin='top')
        my, mx = mag_tools.stripes(origin='bottom')
        my, mx = mag_tools.stripes(pad_width=None)
        my, mx = mag_tools.stripes(pad_width=50)
        my, mx = mag_tools.stripes(h2h=True)
        my, mx = mag_tools.stripes(nstripes=8)
        plt.close('all')
    
    def test_uniform(self):
        my, mx = mag_tools.uniform()
        my, mx = mag_tools.uniform(shape=(256,)*2)
        my, mx = mag_tools.uniform(plot=True)
        my, mx = mag_tools.uniform(origin='top')
        my, mx = mag_tools.uniform(origin='bottom')
        my, mx = mag_tools.uniform(pad_width=None)
        my, mx = mag_tools.uniform(pad_width=50)
        plt.close('all')
    
    def test_grad(self):
        my, mx = mag_tools.grad()
        my, mx = mag_tools.grad(shape=(256,)*2)
        my, mx = mag_tools.grad(plot=True)
        my, mx = mag_tools.grad(origin='top')
        my, mx = mag_tools.grad(origin='bottom')
        my, mx = mag_tools.grad(pad_width=None)
        my, mx = mag_tools.grad(pad_width=50)
        plt.close('all')
    
    def test_divergent(self):
        my, mx = mag_tools.divergent()
        my, mx = mag_tools.divergent(shape=(256,)*2)
        my, mx = mag_tools.divergent(plot=True)
        my, mx = mag_tools.divergent(origin='top')
        my, mx = mag_tools.divergent(origin='bottom')
        my, mx = mag_tools.divergent(pad_width=None)
        my, mx = mag_tools.divergent(pad_width=50)
        plt.close('all')
    
    def test_divergent(self):
        my, mx = mag_tools.neel()
        my, mx = mag_tools.neel(shape=(256,)*2)
        my, mx = mag_tools.neel(plot=True)
        my, mx = mag_tools.neel(origin='top')
        my, mx = mag_tools.neel(origin='bottom')
        my, mx = mag_tools.neel(pad_width=None)
        my, mx = mag_tools.neel(pad_width=50)
        my, mx = mag_tools.neel(width=32)
        plt.close('all')
    
    def test_mag_phase(self):
        my, mx = mag_tools.landau()
        p = mag_tools.mag_phase(my, mx, thickness=10e-9, phase_amp=10, plot=False)
        p = mag_tools.mag_phase(my, mx, thickness=10e-9, phase_amp=10, plot=True)
        plt.close('all')
    
    def test_analytical(self):
        my, mx = mag_tools.landau()
        p = mag_tools.mag_phase(my, mx, thickness=10e-9, phase_amp=10, plot=False)
        
        phase_grad0 = np.percentile(p.phase_grady, 97)
        bt = mag_tools.phasegrad2bt(phase_grad0)
        phase_grad1 = mag_tools.bt2phasegrad(bt)
        assert np.isclose(phase_grad0, phase_grad1)

        beta = mag_tools.bt2beta(bt)
        phase_grady2 = mag_tools.beta2phasegrad(beta)
        assert np.isclose(phase_grady2, phase_grad0)
        
        bt2 = mag_tools.beta2bt(beta)
        assert np.allclose(bt, bt2)
        
        assert np.allclose(mag_tools.tesla2mag(mag_tools.mag2tesla(1)), 1)
        assert np.allclose(mag_tools.tesla2G(1), 1e4)
        assert np.allclose(mag_tools.tesla2Oe(1), 1e4)
        
        period = mag_tools.phasegrad2period(phase_grad0)


if __name__ == '__main__':
    unittest.main()






















