from __future__ import print_function
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
import unittest
import numpy as np

import fpd


class TestSyntheticData(unittest.TestCase):       
    def test_fpd_data_view(self):
        im = np.ones((32,)*2)
        data = fpd.synthetic_data.fpd_data_view(im, (32,)*2)
        
        im_mem_pointer, im_read_only = im.__array_interface__['data']
        data_mem_pointer, data_read_only = data.__array_interface__['data']
        
        self.assertTrue((im_mem_pointer == data_mem_pointer) and data_read_only==True)
        
if __name__ == '__main__':
    unittest.main()


